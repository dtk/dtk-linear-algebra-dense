// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMatrixTest.h"

#include <dtkLinearAlgebraDense>

// This must match the default for PreallocSize.
static const qlonglong ExpectedMinCapacity = 0;

// Exception type that is thrown by ComplexValue.
class ComplexValueException
{
public:
    ComplexValueException(int value, bool inCtor)
        : m_value(value), m_inCtor(inCtor) {}

    int value() const { return m_value; }
    bool inConstructor() const { return m_inCtor; }

private:
    int m_value;
    bool m_inCtor;
};

// Complex type that helps the tests determine if QArray is calling
// constructors, destructors, and copy constructors in the right places.
class ComplexValue
{
public:
    enum Mode
    {
        Default,
        Init,
        Copy,
        CopiedAgain,
        Assign,
        ThrowInCtor,
        ThrowOnCopy
    };

    static int destroyCount;

    ComplexValue() : m_value(-1), m_mode(Default) {}
    ComplexValue(int value) : m_value(value), m_mode(Init) {}
#ifndef QT_NO_EXCEPTIONS
    ComplexValue(int value, Mode mode) : m_value(value), m_mode(mode)
    {
        if (mode == ThrowInCtor)
            throw new ComplexValueException(value, true);
    }
#endif
    ComplexValue(const ComplexValue& other)
        : m_value(other.m_value)
    {
        if (other.m_mode == Copy || other.m_mode == CopiedAgain)
            m_mode = CopiedAgain;
#ifndef QT_NO_EXCEPTIONS
        else if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        else
            m_mode = Copy;
    }
    ~ComplexValue() { ++destroyCount; }

    ComplexValue& operator=(const ComplexValue& other)
    {
#ifndef QT_NO_EXCEPTIONS
        if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        m_value = other.m_value;
        m_mode = Assign;
        return *this;
    }

    int value() const { return m_value; }
    Mode mode() const { return m_mode; }

    bool operator==(const ComplexValue& other) const
        { return m_value == other.m_value; }
    bool operator==(int other) const
        { return m_value == other; }
    bool operator!=(const ComplexValue& other) const
        { return m_value != other.m_value; }
    bool operator!=(int other) const
        { return m_value != other; }

private:
    int m_value;
    Mode m_mode;
};

int ComplexValue::destroyCount = 0;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkMatrixTestCase::initTestCase(void)
{

}

void dtkMatrixTestCase::init(void)
{

}

void dtkMatrixTestCase::testCreate(void)
{
    // Check the basic properties.
    dtkDenseMatrix<double> m0;

    QCOMPARE(m0.numRows(), 0LL);
    QCOMPARE(m0.numCols(), 0LL);

    //
    dtkDenseMatrix<double> m1(9, 9);

    QCOMPARE(m1.numRows(), 9LL);
    QCOMPARE(m1.numCols(), 9LL);
    QVERIFY(m1.data() != 0);

    double init_value = qSqrt(2);
    dtkDenseMatrix<double> m2(121, 11);
    m2.fill(init_value);



    QCOMPARE(m2.numRows(), 121LL);
    QCOMPARE(m2.numCols(), 11LL);
    QVERIFY(m2.data() != 0);

    for (qlonglong i = 1; i < 121 * 11; ++i)
        QCOMPARE(m2.data()[i], init_value);
}

void dtkMatrixTestCase::testViews(void)
{
    dtkUnderscore _;
    dtkDenseMatrix<double> m0(4,4);
    m0.fill(1);
    dtkDenseMatrix<double>::View m0_view=m0(_(1,2), _(2,3));
    m0_view.fill(42);

    for(qlonglong j=1;j<5;j++)
    {
        for(qlonglong i=1;i<5;i++)
        {
            if((i==1 || i==2) && (j==2 || j==3))
                QVERIFY(m0(i,j)==42);
            else
                QVERIFY(m0(i,j)==1);
        }
    }

    //test stride
    dtkDenseMatrix<double> m1(3,3);
    dtkDenseMatrix<double>::View m1_view=m1(_, _(1,2,3));

    m1.fill(1);
    m1_view.fill(42);

    for(qlonglong j=1;j<4;j++)
    {
        for(qlonglong i=1;i<4;i++)
        {
            if(j==1 || j==3)
                QVERIFY(m1(i,j)==42);
            else
                QVERIFY(m1(i,j)==1);
        }
    }

    dtkDenseMatrix<double> m2(11,11);
    dtkDenseMatrix<double>::View m2_view=m2(_(2,3), _(3,2,7));

    m2.fill(1);
    m2_view.fill(42);

    for(qlonglong j=1;j<12;j++)
    {
        for(qlonglong i=1;i<12;i++)
        {
            if((j==3 || j==5 || j==7) && ((i==2)||(i==3)))
                QVERIFY(m2(i,j)==42);
            else
                QVERIFY(m2(i,j)==1);
        }
    }
    //test column


}

void dtkMatrixTestCase::testAddAssign(void)
{
    dtkUnderscore _;
    dtkDenseMatrix<double> a0(4,4),b0(4,4);

    a0.fill(2);
    b0.fill(40);

    a0+=b0;

    for(qlonglong j=1;j<5;j++)
    {
        for(qlonglong i=1;i<5;i++)
        {
           QVERIFY(a0(i,j)==42);
           QVERIFY(b0(i,j)==40);
        }
    }
}

void dtkMatrixTestCase::testSubAssign(void)
{
    dtkUnderscore _;
    dtkDenseMatrix<double> a0(4,4),b0(4,4);

    a0.fill(82);
    b0.fill(40);

    a0-=b0;

    for(qlonglong j=1;j<5;j++)
    {
        for(qlonglong i=1;i<5;i++)
        {
           QVERIFY(a0(i,j)==42);
           QVERIFY(b0(i,j)==40);
        }
    }
}

void dtkMatrixTestCase::testMultAssign(void)
{
    dtkUnderscore _;
    dtkDenseMatrix<double> a0(4,4),b0(4,4),c0(4,4);

    a0.fill(3);
    b0.fill(2);
    c0=a0*b0;

    a0*=5;

    for(qlonglong j=1;j<5;j++)
    {
        for(qlonglong i=1;i<5;i++)
        {
           QVERIFY(a0(i,j)==15);
           QVERIFY(b0(i,j)==2);
           QVERIFY(c0(i,j)==24);
        }
    }
}

void dtkMatrixTestCase::testResize(void)
{
    dtkDenseMatrix<double> a0(4,4);
    const double* place=a0.data();
    a0.fill(5);
    a0.resize(4,4);
    QVERIFY(a0.data()==place);

    a0.resize(5,5);
    QCOMPARE(a0.numRows(), 5LL);
    QCOMPARE(a0.numCols(), 5LL);

    a0.resize(3,3);
    QCOMPARE(a0.numRows(), 3LL);
    QCOMPARE(a0.numCols(), 3LL);
}

void dtkMatrixTestCase::testDataStream(void)
{
    dtkDenseMatrix<double> m(121, 89);
    flens::fillRandom(m);

    QByteArray data;
    {
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream << m;
    }

    dtkDenseMatrix<double> r;
    {
        QDataStream stream2(data);
        stream2 >> r;
    }

    QCOMPARE(r.numRows(), m.numRows());
    QCOMPARE(r.numCols(), m.numCols());

    const double *m_it = m.data();
    const double *m_end = m.data() + m.numRows() * m.numCols();

    const double *r_it = r.data();

    for(; m_it != m_end; ++m_it, ++r_it) {
        QCOMPARE(*r_it, *m_it);
    }
}

void dtkMatrixTestCase::cleanupTestCase(void)
{

}

void dtkMatrixTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkMatrixTest, dtkMatrixTestCase)

//
// dtkMatrixTest.cpp ends here
