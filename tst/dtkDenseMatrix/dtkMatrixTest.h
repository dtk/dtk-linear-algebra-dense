// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkMatrixTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreate(void);
    void testViews(void);
    void testAddAssign(void);
    void testSubAssign(void);
    void testMultAssign(void);
    void testResize(void);
    void testDataStream(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkMatrixTest.h ends here
