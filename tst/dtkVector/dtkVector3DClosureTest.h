// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

class dtkVector3DClosureTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testAdd(void);
    void testSub(void);
    void testScal(void);
    void testRScal(void);
    void testDot(void);
    void testCross(void);
    void testTripleScalaraProduct(void);
    void testWildExpr(void);
    void testMv(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkVector3DClosureTest.h ends here
