// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkStaticVectorTest.h"

#include <dtkLinearAlgebraDense>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkStaticVectorTestCase::initTestCase(void)
{

}

void dtkStaticVectorTestCase::init(void)
{

}

void dtkStaticVectorTestCase::testCreate(void)
{
    {
        dtkStaticVector<double, 101> v;
        QCOMPARE(v.size(), 101UL);
        QCOMPARE(v.length(), 101UL);
        QCOMPARE(v.count(), 101UL);
        QCOMPARE(v.stride(), 1UL);
        QCOMPARE(v.firstIndex(), 1UL);
        QCOMPARE(v.lastIndex(), 101UL);
    }

    {
        dtkStaticVectorEngine<double, 101> engine;
        for(qlonglong i = 0; i < engine.length; ++i) {
            engine.data()[i] = i;
        }

        dtkStaticVector<double, 101> v(engine);
        QCOMPARE(v.size(), 101UL);
        QCOMPARE(v.length(), 101UL);
        QCOMPARE(v.count(), 101UL);
        QCOMPARE(v.stride(), 1UL);
        QCOMPARE(v.firstIndex(), 1UL);
        QCOMPARE(v.lastIndex(), 101UL);

        for(qlonglong i = 0; i < v.size(); ++i) {
            QCOMPARE(v.data()[i], 1.0 * i);
        }
    }

    {
        dtkStaticVector<double, 7> v({1, 2, 3, 4, 5, 6, 7});
        QCOMPARE(v.size(), 7UL);
        QCOMPARE(v.length(), 7UL);
        QCOMPARE(v.stride(), 1UL);
        QCOMPARE(v.firstIndex(), 1UL);
        QCOMPARE(v.lastIndex(), 7UL);

        dtkStaticVector<double, 7> v_copy(v);
        QCOMPARE(v_copy.size(), 7UL);
        QCOMPARE(v_copy.length(), 7UL);
        QCOMPARE(v_copy.stride(), 1UL);
        QCOMPARE(v_copy.firstIndex(), 1UL);
        QCOMPARE(v_copy.lastIndex(), 7UL);
    }

    {
        dtkFlensTinyVector< flens::TinyArray<double, 11> > fv;

        dtkStaticVector<double, 11> v(fv);
        QCOMPARE(v.size(), 11UL);
        QCOMPARE(v.length(), 11UL);
        QCOMPARE(v.stride(), 1UL);
        QCOMPARE(v.firstIndex(), 1UL);
        QCOMPARE(v.lastIndex(), 11UL);
    }
}

void dtkStaticVectorTestCase::testSetAndGetValue(void)
{
    {
        std::initializer_list<double> list = {1, 2, 3, 4, 5, 6, 7};
        dtkStaticVector<double, 7> v(list);

        auto it = list.begin();
        for(qlonglong i = 0; i < v.size(); ++i, ++it) {
            QCOMPARE(v.data()[i], *it);
        }
    }

    {
        std::initializer_list<double> list = {1, 2, 3, 4, 5, 6, 7};
        dtkStaticVector<double, 7> v;

        for(qlonglong i = 0; i < v.size(); ++i) {
            v.data()[i] = i + 1;
        }
        auto it = list.begin();
        for(qlonglong i = 0; i < v.size(); ++i, ++it) {
            QCOMPARE(v.data()[i], *it);
        }
    }
}

void dtkStaticVectorTestCase::testFill(void)
{
    {
        double pi = 3.14159;
        dtkStaticVector<double, 101> v;
        v.fill(pi);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi);
        }
    }
}

void dtkStaticVectorTestCase::testAssign(void)
{
    {
        dtkFlensTinyVector< flens::TinyArray<double, 11> > fv;
        for(qlonglong i = fv.firstIndex(); i <= fv.lastIndex(); ++i) {
            fv(i) = i;
        }

        dtkStaticVector<double, 11> v; v = fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), fv(i));
        }
    }

    {
        dtkStaticVector<double, 11> fv = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

        dtkStaticVector<double, 11> v; v = fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), fv(i));
        }
    }
}

void dtkStaticVectorTestCase::testAddAssign(void)
{
    {
        dtkFlensTinyVector< flens::TinyArray<double, 11> > fv;
        for(qlonglong i = fv.firstIndex(); i <= fv.lastIndex(); ++i) {
            fv(i) = i;
        }

        dtkStaticVector<double, 11> v;
        double pi = 3.14159;
        v.fill(pi);
        v += fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi + fv(i));
        }
    }

    {
        dtkStaticVector<double, 11> fv = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

        dtkStaticVector<double, 11> v;
        double pi = 3.14159;
        v.fill(pi);
        v += fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi + fv(i));
        }
    }
}

void dtkStaticVectorTestCase::testSubAssign(void)
{
    {
        dtkFlensTinyVector< flens::TinyArray<double, 11> > fv;
        for(qlonglong i = fv.firstIndex(); i <= fv.lastIndex(); ++i) {
            fv(i) = i;
        }

        dtkStaticVector<double, 11> v;
        double pi = 3.14159;
        v.fill(pi);
        v -= fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi - fv(i));
        }
    }
}

void dtkStaticVectorTestCase::testAssignValue(void)
{
    {
        double pi = 3.14159;
        dtkStaticVector<double, 101> v;
        v = pi;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi);
        }
        v+= pi;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), 2 * pi);
        }
        v-= pi;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi);
        }
    }
}

void dtkStaticVectorTestCase::testScal(void)
{
    {
        double pi = 3.14159;
        dtkStaticVector<double, 101> v;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            v(i) = i;
        }

        v *= pi;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi * i);
        }
    }

    {
        double pi = 3.14159;
        dtkStaticVector<double, 101> v;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            v(i) = i;
        }

        v /= pi;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), i / pi);
        }
    }
}

void dtkStaticVectorTestCase::testData(void)
{
    {
        const dtkStaticVector<double, 7> v({1, 2, 3, 4, 5, 6, 7});
        const double *buf = v.data();

        QVERIFY(buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(buf + i - 1));
        }
    }
    {
        dtkStaticVector<double, 7> v({1, 2, 3, 4, 5, 6, 7});
        double *buf = v.data();

        QVERIFY(buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(buf + i - 1));
        }

        const double *c_buf = v.constData();

        QVERIFY(c_buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(c_buf + i - 1));
        }
    }
}

void dtkStaticVectorTestCase::cleanupTestCase(void)
{

}

void dtkStaticVectorTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkStaticVectorTest, dtkStaticVectorTestCase)

//
// dtkStaticVectorTest.cpp ends here
