// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkStaticVectorTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreate(void);
    void testFill(void);
    void testSetAndGetValue(void);
    void testAssign(void);
    void testAddAssign(void);
    void testSubAssign(void);
    void testAssignValue(void);
    void testScal(void);
    void testData(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkStaticVectorTest.h ends here
