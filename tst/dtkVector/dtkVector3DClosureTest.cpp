// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVector3DClosureTest.h"

#include <dtkLinearAlgebraDense>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkVector3DClosureTestCase::initTestCase(void)
{

}

void dtkVector3DClosureTestCase::init(void)
{

}

void dtkVector3DClosureTestCase::testAdd(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2; v2.fill(0);

    v2 = v1 + v0;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v2(i) , (v0(i) + v1(i)));
    }

    v2 += v1; v2 += v0;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v2(i) , 2*(v0(i) + v1(i)));
    }
}

void dtkVector3DClosureTestCase::testSub(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2; v2.fill(0);

    v2 = v0 - v1;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v2(i) , (v0(i) - v1(i)));
    }

    v2 -= v0;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v2(i) , (- v1(i)));
    }
}

void dtkVector3DClosureTestCase::testScal(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;

    double s = 0.5;

    v1 = s * v0;
    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v1(i) , s * v0(i));
    }

    v1 += s * v0;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v1(i) , 2 * s * v0(i));
    }
}

void dtkVector3DClosureTestCase::testRScal(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;

    double s = 4.15689;

    v1 = v0 / s;
    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v1(i) , v0(i) / s);
    }

    v1 += v0 / s;

    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v1(i) , 2 * v0(i) / s);
    }
}

void dtkVector3DClosureTestCase::testDot(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2; v2.fill(0);

    double s = v0 * v1;


    double ss = 0;
    for (int i = 1; i < 4; ++i) {
        ss += v0(i) * v1(i);
    }
    QCOMPARE(s, ss);

    v2 = (v0 * v1) * v0;
    for (int i = 1; i < 4; ++i) {
        QCOMPARE(v2(i), ss * v0(i));
    }
}

void dtkVector3DClosureTestCase::testCross(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2; v2.fill(0);

    v2 = v0 % v1;

    QCOMPARE(v2(1), (v0(2) * v1(3) - v0(3) * v1(2)));
    QCOMPARE(v2(2), (v0(3) * v1(1) - v0(1) * v1(3)));
    QCOMPARE(v2(3), (v0(1) * v1(2) - v0(2) * v1(1)));
}

void dtkVector3DClosureTestCase::testTripleScalaraProduct(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2; v2.fill(0);

    v2 = v0 % v1;

    QCOMPARE(v2(1), (v0(2) * v1(3) - v0(3) * v1(2)));
    QCOMPARE(v2(2), (v0(3) * v1(1) - v0(1) * v1(3)));
    QCOMPARE(v2(3), (v0(1) * v1(2) - v0(2) * v1(1)));

    v2.fill(1);
    v2 -= v0 % v1;

    QCOMPARE(v2(1), 1. - (v0(2) * v1(3) - v0(3) * v1(2)));
    QCOMPARE(v2(2), 1. - (v0(3) * v1(1) - v0(1) * v1(3)));
    QCOMPARE(v2(3), 1. - (v0(1) * v1(2) - v0(2) * v1(1)));

    double tsp = v0 * (v1 % v2);

    double check  = v2(1) * (v0(2) * v1(3) - v0(3) * v1(2));
           check += v2(2) * (v0(3) * v1(1) - v0(1) * v1(3));
           check += v2(3) * (v0(1) * v1(2) - v0(2) * v1(1));

    QCOMPARE(tsp, check);
}

void dtkVector3DClosureTestCase::testWildExpr(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    dtkVector3D<double> v1;
    v1(1) = 0.76420;
    v1(2) = 1;
    v1(3) = 11.74646;

    dtkVector3D<double> v2;

    double s = 0.249563;

    v2 = s * v0 % v1 + s * v1 - v0 / s;

    dtkVector3D<double> vv2;

    vv2 = v0 % v1;
    vv2 *= s;
    vv2 += s * v1;
    vv2 -= v0 / s;

    QCOMPARE(v2(1), vv2(1));
    QCOMPARE(v2(2), vv2(2));
    QCOMPARE(v2(3), vv2(3));
}

void dtkVector3DClosureTestCase::testMv(void)
{
    dtkVector3D<double> v0;
    v0(1) = 1.1;
    v0(2) = 0.4357;
    v0(3) = 3.14159;

    flens::GeTinyMatrix< flens::TinyFullStorage<double, 3, 3> > mat;

    mat(1,3) = 1;

    dtkVector3D<double> v1;

    v1 = mat * v0;

    qDebug() << v1;
}

void dtkVector3DClosureTestCase::cleanupTestCase(void)
{

}

void dtkVector3DClosureTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkVector3DClosureTest, dtkVector3DClosureTestCase)

//
// dtkVector3DClosureTest.cpp ends here
