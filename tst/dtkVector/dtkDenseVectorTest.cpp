// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDenseVectorTest.h"

#include <dtkLinearAlgebraDense>
#include <cmath>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkDenseVectorTestCase::initTestCase(void)
{

}

void dtkDenseVectorTestCase::init(void)
{

}

void dtkDenseVectorTestCase::testCreate(void)
{
    {
        dtkDenseVector<double> v;
        QVERIFY(!v.length());
    }

    {
        dtkDenseVector<double> v(101);
        QCOMPARE(v.length(), 101LL);
        QCOMPARE(v.stride(), 1LL);
        QCOMPARE(v.firstIndex(), 1LL);
        QCOMPARE(v.lastIndex(), 101LL);
    }

    {
        dtkDenseVector<double> v(7); 
        v = 1, 2, 3, 4, 5, 6, 7;
        QCOMPARE(v.length(), 7LL);
        QCOMPARE(v.stride(), 1LL);
        QCOMPARE(v.firstIndex(), 1LL);
        QCOMPARE(v.lastIndex(), 7LL);

        dtkDenseVector<double> v_copy(v);
        QCOMPARE(v_copy.length(), 7LL);
        QCOMPARE(v_copy.stride(), 1LL);
        QCOMPARE(v_copy.firstIndex(), 1LL);
        QCOMPARE(v_copy.lastIndex(), 7LL);
    }

    {
        flens::DenseVector< flens::Array<double> > fv(11);
        fv = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;

        dtkDenseVector<double> v(fv);
        QCOMPARE(v.length(), 11LL);
        QCOMPARE(v.stride(), 1LL);
        QCOMPARE(v.firstIndex(), 1LL);
        QCOMPARE(v.lastIndex(), 11LL);
    }
}

void dtkDenseVectorTestCase::testGetValue(void)
{
    {
        std::initializer_list<double> list = {1, 2, 3, 4, 5, 6, 7};
        auto it = list.begin();
        dtkDenseVector<double> v(list.size());
        for(int i = 0; i < list.size(); ++i, ++it) {
            v(i+1) = *it;
        }
        it = list.begin();

        for(qlonglong i = 1; i <= v.length(); ++i, ++it) {
            QCOMPARE(v(i), *it);
        }

        const dtkDenseVector<double> v_copy(v);
        it = list.begin();

        for(qlonglong i = 1; i <= v_copy.length(); ++i, ++it) {
            QCOMPARE(v_copy(i), *it);
        }
    }
}

void dtkDenseVectorTestCase::testSetValue(void)
{
    {
        std::initializer_list<double> list = {1, 2, 3, 4, 5, 6, 7};
        auto it = list.begin();
        dtkDenseVector<double> v(7);

        for(qlonglong i = 1; i <= v.length(); ++i, ++it) {
            v(i) = *it;
            QCOMPARE(v(i), *it);
        }
    }
}

void dtkDenseVectorTestCase::testResize(void)
{
    {
        dtkDenseVector<double> v(7);
        dtkDenseVector<double> w(v);
        w.resize(11);
        QCOMPARE(w.length(), 11LL);
        QCOMPARE(w.stride(), 1LL);
        QCOMPARE(w.firstIndex(), 1LL);
        QCOMPARE(w.lastIndex(), 11LL);
    }
}

void dtkDenseVectorTestCase::testFill(void)
{
    {
        double pi = 3.14159;
        dtkDenseVector<double> v(101);
        v.fill(pi);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi);
        }
    }
}

void dtkDenseVectorTestCase::testAssign(void)
{
    {
        flens::DenseVector< flens::Array<double> > fv(11);
        fv = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;

        dtkDenseVector<double> v; v = fv;
        QCOMPARE(v.length(), 11LL);
        QCOMPARE(v.stride(), 1LL);
        QCOMPARE(v.firstIndex(), 1LL);
        QCOMPARE(v.lastIndex(), 11LL);

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), fv(i));
        }
    }

    {
        dtkDenseVector<double> fv(11); 
        fv = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;

        dtkDenseVector<double> v; v = fv;
        QCOMPARE(v.length(), 11LL);
        QCOMPARE(v.stride(), 1LL);
        QCOMPARE(v.firstIndex(), 1LL);
        QCOMPARE(v.lastIndex(), 11LL);

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), fv(i));
        }
    }
}

void dtkDenseVectorTestCase::testAddAssign(void)
{
    {
        flens::DenseVector< flens::Array<double> > fv(11);
        fv = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;

        dtkDenseVector<double> v(11);
        double pi = 3.14159;
        v.fill(pi);
        v += fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi + fv(i));
        }
    }
}

void dtkDenseVectorTestCase::testSubAssign(void)
{
    {
        flens::DenseVector< flens::Array<double> > fv(11);
        fv = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;

        dtkDenseVector<double> v(11);
        double pi = 3.14159;
        v.fill(pi);
        v -= fv;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi - fv(i));
        }
    }
}

void dtkDenseVectorTestCase::testAssignValue(void)
{
    {
        double pi = 3.14159;
        dtkDenseVector<double> v(101);
        v = pi;
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi);
        }
    }
}

void dtkDenseVectorTestCase::testScal(void)
{
    {
        double pi = 3.14159;
        dtkDenseVector<double> v(101);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            v(i) = i;
        }

        v *= pi;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), pi * i);
        }
    }

    {
        double pi = 3.14159;
        dtkDenseVector<double> v(101);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            v(i) = i;
        }

        v /= pi;

        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), i / pi);
        }
    }
}

void dtkDenseVectorTestCase::testData(void)
{
    {
        dtkDenseVector<double> ref(7); ref = 1, 2, 3, 4, 5, 6, 7;
        const dtkDenseVector<double> v(ref);
        const double *buf = v.data();

        QVERIFY(buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(buf + i - 1));
        }
    }

    {
        dtkDenseVector<double> v(7); v = 1, 2, 3, 4, 5, 6, 7;
        double *buf = v.data();

        QVERIFY(buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(buf + i - 1));
        }

        const double *c_buf = v.data();

        QVERIFY(c_buf);
        for(qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i) {
            QCOMPARE(v(i), *(c_buf + i - 1));
        }
    }
}

void dtkDenseVectorTestCase::testView(void)
{
    dtkUnderscore _;
    typedef typename dtkDenseVector<double>::View View;

    {
        dtkDenseVector<double> v(7); v = 1, 2, 3, 4, 5, 6, 7;
        View view = v(_(3, 6));

        QCOMPARE(view.length(), 4LL);
        QCOMPARE(view.stride(), 1LL);
        QCOMPARE(view.firstIndex(), 1LL);
        QCOMPARE(view.lastIndex(), 4LL);

        for(qlonglong i = view.firstIndex(); i <= view.lastIndex(); ++i) {
            QCOMPARE(view(i), v(i + 2));
        }

        double pi = 3.14159;
        for(qlonglong i = view.firstIndex(); i <= view.lastIndex(); ++i) {
            view(i) = pi + i;
        }

        for(qlonglong i = view.firstIndex(); i <= view.lastIndex(); ++i) {
            QCOMPARE(v(i + 2), pi + i);
        }
    }

    {
        dtkDenseVector<double> v(7); v = 1, 2, 3, 4, 5, 6, 7;
        View view = v(_(3, 2, 6));

        QCOMPARE(view.length(), 2LL);
        QCOMPARE(view.stride(), 2LL);
        QCOMPARE(view.firstIndex(), 1LL);
        QCOMPARE(view.lastIndex(), 2LL);

        for(qlonglong i = view.firstIndex(); i <= view.lastIndex(); ++i) {
            QCOMPARE(view(i), v(3 + 2 * (i-1)));
        }

        dtkDenseVector<double> ref = v;
        dtkDenseVector<double> w(2); w = 3.14159, 0.33333;
        view += w;

        for(qlonglong i = view.firstIndex(); i <= view.lastIndex(); ++i) {
            QCOMPARE(view(i), ref(3 + 2 * (i-1)) + w(i));
        }
    }

    {
        dtkDenseVector<double> v(7); v = 1, 2, 3, 4, 5, 6, 7;
        View view = v(_(1, 2, 7));

        View view2 = view(_(view.firstIndex(), 2, view.lastIndex()));

        QCOMPARE(view2.length(), 2LL);
        QCOMPARE(view2.stride(), 4LL);
        QCOMPARE(view2.firstIndex(), 1LL);
        QCOMPARE(view2.lastIndex(), 2LL);

        for(qlonglong i = view2.firstIndex(); i <= view2.lastIndex(); ++i) {
            QCOMPARE(view2(i), v(1 + 4 * (i-1)));
        }
    }
}

void dtkDenseVectorTestCase::testConstView(void)
{
    dtkUnderscore _;
    typedef typename dtkDenseVector<double>::View           View;
    typedef typename dtkDenseVector<double>::ConstView ConstView;

    {
        dtkDenseVector<double> ref(7); ref = 1, 2, 3, 4, 5, 6, 7;
        const dtkDenseVector<double> v(ref);
        const ConstView cview = v(_(3, 6));

        QCOMPARE(cview.length(), 4LL);
        QCOMPARE(cview.stride(), 1LL);
        QCOMPARE(cview.firstIndex(), 1LL);
        QCOMPARE(cview.lastIndex(), 4LL);

        for(qlonglong i = cview.firstIndex(); i <= cview.lastIndex(); ++i) {
            QCOMPARE(cview(i), v(i + 2));
        }
    }

    {
        dtkDenseVector<double> v(7); v = 1, 2, 3, 4, 5, 6, 7;
        const ConstView cview = v(_(3, 6));

        QCOMPARE(cview.length(), 4LL);
        QCOMPARE(cview.stride(), 1LL);
        QCOMPARE(cview.firstIndex(), 1LL);
        QCOMPARE(cview.lastIndex(), 4LL);

        for(qlonglong i = cview.firstIndex(); i <= cview.lastIndex(); ++i) {
            QCOMPARE(cview(i), v(i + 2));
        }
    }
}

void dtkDenseVectorTestCase::testElementWise(void)
{
    dtkDenseVector<double> v(5);

    dtkDenseVector<double>::IndexVariable i;

    v(i) = sqrt(2.) * i;

    for (qlonglong j = 1; j < v.endIndex(); ++j)
        QCOMPARE(v(j), sqrt(2.) * j);
}

void dtkDenseVectorTestCase::testRawData(void)
{
    double buffer[5] = { 1., 2.3, 3, 4.67, 8.94764 };

    const dtkDenseVector<double>::ConstView cv = dtkDenseVectorFromRawData(5LL, buffer);

    for (qlonglong i = cv.firstIndex(); i <= cv.lastIndex(); ++i)
        QCOMPARE(cv(i), buffer[i-1LL]);

    double pi = 3.14159;

    dtkDenseVector<double>::View v = dtkDenseVectorFromWritableRawData(5LL, buffer);

    for (qlonglong i = v.firstIndex(); i <= v.lastIndex(); ++i)
        QCOMPARE(v(i), buffer[i-1LL]);

    v = pi;

    for (qlonglong i = 0; i < 5LL; ++i)
        QCOMPARE(buffer[i], pi);
}

void dtkDenseVectorTestCase::testDataStream(void)
{
    dtkDenseVector<double> v(89);
    flens::fillRandom(v);

    QByteArray data;
    {
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream << v;
    }

    dtkDenseVector<double> r;
    {
        QDataStream stream2(data);
        stream2 >> r;
    }

    QCOMPARE(r.length(), v.length());

    const double *v_it  = v.data();
    const double *v_end = v.data() + v.length();

    const double *r_it = r.data();

    for(; v_it != v_end; ++v_it, ++r_it) {
        QCOMPARE(*r_it, *v_it);
    }
}

void dtkDenseVectorTestCase::cleanupTestCase(void)
{

}

void dtkDenseVectorTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkDenseVectorTest, dtkDenseVectorTestCase)

//
// dtkDenseVectorTest.cpp ends here
