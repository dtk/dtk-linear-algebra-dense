// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>


class dtkMatrixOperationsTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testSvd(void);
    void testCond(void);
    void testSolve(void);
    void testPinv(void);
    void testNorm(void);
    void testLU(void);
    void testFromDiag(void);
    void testEltWiseMult(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkMatrixOperationsTest.h ends here
