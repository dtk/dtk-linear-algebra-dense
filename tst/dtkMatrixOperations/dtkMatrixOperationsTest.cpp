// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMatrixOperationsTest.h"

#include <dtkLinearAlgebraDense>

// This must match the default for PreallocSize.
static const qlonglong ExpectedMinCapacity = 0;

// Exception type that is thrown by ComplexValue.
class ComplexValueException
{
public:
    ComplexValueException(int value, bool inCtor)
        : m_value(value), m_inCtor(inCtor) {}

    int value() const { return m_value; }
    bool inConstructor() const { return m_inCtor; }

private:
    int m_value;
    bool m_inCtor;
};

// Complex type that helps the tests determine if QArray is calling
// constructors, destructors, and copy constructors in the right places.
class ComplexValue
{
public:
    enum Mode
    {
        Default,
        Init,
        Copy,
        CopiedAgain,
        Assign,
        ThrowInCtor,
        ThrowOnCopy
    };

    static int destroyCount;

    ComplexValue() : m_value(-1), m_mode(Default) {}
    ComplexValue(int value) : m_value(value), m_mode(Init) {}
#ifndef QT_NO_EXCEPTIONS
    ComplexValue(int value, Mode mode) : m_value(value), m_mode(mode)
    {
        if (mode == ThrowInCtor)
            throw new ComplexValueException(value, true);
    }
#endif
    ComplexValue(const ComplexValue& other)
        : m_value(other.m_value)
    {
        if (other.m_mode == Copy || other.m_mode == CopiedAgain)
            m_mode = CopiedAgain;
#ifndef QT_NO_EXCEPTIONS
        else if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        else
            m_mode = Copy;
    }
    ~ComplexValue() { ++destroyCount; }

    ComplexValue& operator=(const ComplexValue& other)
    {
#ifndef QT_NO_EXCEPTIONS
        if (other.m_mode == ThrowOnCopy)
            throw new ComplexValueException(other.m_value, false);
#endif
        m_value = other.m_value;
        m_mode = Assign;
        return *this;
    }

    int value() const { return m_value; }
    Mode mode() const { return m_mode; }

    bool operator==(const ComplexValue& other) const
        { return m_value == other.m_value; }
    bool operator==(int other) const
        { return m_value == other; }
    bool operator!=(const ComplexValue& other) const
        { return m_value != other.m_value; }
    bool operator!=(int other) const
        { return m_value != other; }

private:
    int m_value;
    Mode m_mode;
};

int ComplexValue::destroyCount = 0;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkMatrixOperationsTestCase::initTestCase(void)
{

}

void dtkMatrixOperationsTestCase::init(void)
{

}

void dtkMatrixOperationsTestCase::testSvd(void)
{
    dtkUnderscore _;

    dtkDenseMatrix<double> a(4,4);
    dtkDenseVector<double> s(4);
    dtkDenseMatrix<double> u(4,4);
    dtkDenseMatrix<double> v(4,4);

    a(1,_) = 8., 6., 9., 9.;
    a(2,_) = 9., 0., 9., 4.;
    a(3,_) = 1., 2., 1., 8.;
    a(4,_) = 9., 5., 9., 1.;

    dtk::lapack::svd(a,s,u,v);

    dtkDenseVector<double> trueS(4); trueS = 24.5359,8.8472,4.1869,0.4335;
    dtkDenseMatrix<double> trueU(4,4);
    dtkDenseMatrix<double> trueV(4,4);


    trueU(1,_)=-0.6446, 0.3653, 0.2765,-0.6120;
    trueU(2,_)=-0.5169,-0.2669,-0.8132, 0.0177;
    trueU(3,_)=-0.2064, 0.7506,-0.1017, 0.6194;
    trueU(4,_)=-0.5241,-0.4816, 0.5019, 0.4913;


    //watch out, svd returns transpose(V)
    trueV(1,_)=-0.6004,-0.3463,-0.1651, 0.7016;
    trueV(2,_)=-0.2813, 0.1452, 0.9471, 0.0538;
    trueV(3,_)=-0.6267,-0.3050,-0.0990,-0.7102;
    trueV(4,_)=-0.4094, 0.8752,-0.2570, 0.0212;

    v=transpose(v);


    for(int j=1;j<5;j++)
    {
        for(int i=1;i<5;i++)
        {
            QVERIFY(abs(trueU(i,j)-u(i,j))<0.0001);
            QVERIFY(abs(trueV(i,j)-v(i,j))<0.0001);
        }
        QVERIFY(abs(trueS(j)-s(j))<0.0001);
    }
}

void dtkMatrixOperationsTestCase::testCond(void)
{
    dtkUnderscore _;

    dtkDenseMatrix<double> a(4,4);
    a(1,_)=8.,6.,9.,9.;
    a(2,_)=9.,0.,9.,4.;
    a(3,_)=1.,2.,1.,8.;
    a(4,_)=9.,5.,9.,1.;

    double res=dtk::lapack::cond(a);
    QVERIFY(res-56.5986978<1e-7);
}

void dtkMatrixOperationsTestCase::testSolve(void)
{
    dtkUnderscore _;

    //Matrix-vector
    dtkDenseMatrix<double> a(4,4);
    a(1,_)=8.,6.,9.,9.;
    a(2,_)=9.,0.,9.,4.;
    a(3,_)=1.,2.,1.,8.;
    a(4,_)=9.,5.,9.,1.;

    dtkDenseVector<double> b(4); b = 1,2,3,4;
    dtkDenseVector<double> x=dtk::lapack::solve(a,b);

    dtkDenseVector<double> res(4); res = 5.34263959,0.53553299,-5.22081218,0.22588832;

    for(int i=1;i<5;i++)
        QVERIFY(abs(res(i)-x(i))<1e-7);

    dtkDenseMatrix<double> a_(4,3);
    dtkDenseVector<double> b_(4);


    a_(_,1)=1.,0.,0.,0.;
    a_(_,2)=0.,1.,0.,0.;
    a_(_,3)=0.,0.,1.,1.;

    b_ = 1, 2, 2, 1;

    dtkDenseVector<double> res_(3); res_ = 1, 2, 1.5;

    dtkDenseVector<double> x_= dtk::lapack::solve(a_,b_);
    for(int i = 1; i < x_.length(); ++i) {
        QVERIFY((x_(i) - res_(i)) < 1e-7);
    }

}

void dtkMatrixOperationsTestCase::testPinv(void)
{
    dtkUnderscore _;

    dtkDenseMatrix<double> a(4,3);
    a(_,1)=8.,6.,9.,9.;
    a(_,2)=9.,0.,9.,4.;
    a(_,3)=1.,2.,1.,8.;

    dtkDenseMatrix<double> trueRes(3,4);
    trueRes(1,_)=-0.01220742,  0.21122608,  0.03705755, -0.05591279;
    trueRes(2,_)= 0.06825871, -0.18351701,  0.02779811,  0.03387215;
    trueRes(3,_)=-0.01685693, -0.14511406, -0.05903884,  0.17076549;


    dtkDenseMatrix<double> res(3,4);
    dtk::lapack::pinv(a,res);


    QVERIFY(trueRes.numCols()==res.numCols());
    QVERIFY(trueRes.numRows()==res.numRows());

    for(int j=1;j<res.numCols()+1;j++)
    {
        for(int i=1;i<res.numRows()+1;i++)
            QVERIFY(res(i,j)-trueRes(i,j)<10e-7);
    }
}

void dtkMatrixOperationsTestCase::testNorm(void)
{
    dtkUnderscore _;

    dtkDenseMatrix<double> a(4,3);
    a(_,1)=8.,6.,9.,9.;
    a(_,2)=9.,0.,9.,4.;
    a(_,3)=1.,2.,1.,8.;

    double res=dtk::lapack::norm(a);

    QVERIFY(res-21.117679677<1e7);
}

void dtkMatrixOperationsTestCase::testLU(void)
{
    dtkUnderscore _;

    dtkDenseMatrix<double> a(4,4);

    a(1,_)=8.,6.,9.,9.;
    a(2,_)=9.,0.,9.,4.;
    a(3,_)=1.,2.,1.,8.;
    a(4,_)=9.,5.,9.,1.;

    dtkDenseMatrix<double> l(4,4),u(4,4);

    dtk::lapack::lu(a,l,u);

    dtkDenseMatrix<double> trueL(4,4),trueU(4,4);

    trueL(1,_)=  1.        ,  0.        ,  0.        ,  0.         ;
    trueL(2,_)=  0.88888888,  1.        ,  0.        ,  0.         ;
    trueL(3,_)=  1.        ,  0.83333333,  1.        ,  0.         ;
    trueL(4,_)=  0.11111111,  0.33333333,  0.4       ,  1.         ;

    trueU(1,_)=  9.        ,  0.         ,  9.        ,  4.        ;
    trueU(2,_)=  0.        ,  6.         ,  1.        ,  5.44444444;
    trueU(3,_)=  0.        ,  0.         , -0.83333333, -7.53703703;
    trueU(4,_)=  0.        ,  0.         ,  0.        ,  8.75555555;


    for(int j=1;j<trueL.numCols()+1;j++)
    {
        for(int i=1;i<trueL.numRows()+1;i++)
        {
            QVERIFY(trueL(i,j)-l(i,j)<10e-7);
            QVERIFY(trueU(i,j)-u(i,j)<10e-7);
        }
    }
}

void dtkMatrixOperationsTestCase::testFromDiag(void)
{
    dtkUnderscore _;
    dtkDenseVector<double> vec(12);
    vec.fill(5.);

    dtkDenseMatrix<double> mat = dtk::fromDiag(vec);

    for(qlonglong j = 1; j<13; ++j) {
        for(qlonglong i = 1 ; i < 13; ++i) {
            if(i == j)
                QCOMPARE(mat(i,j), 5.);
            else
                QCOMPARE(mat(i,j), 0.);
        }
    }
}

void dtkMatrixOperationsTestCase::testEltWiseMult(void)
{
    dtkUnderscore _;
    dtkDenseMatrix<double> a(4,4);
    a(1,_)=8.,6.,9.,9.;
    a(2,_)=9.,0.,9.,4.;
    a(3,_)=1.,2.,1.,8.;
    a(4,_)=9.,5.,9.,1.;
    dtkDenseMatrix<double> b(4,4);
    b(1,_)=8.,6.,9.,9.;
    b(2,_)=9.,0.,9.,4.;
    b(3,_)=1.,2.,1.,8.;
    b(4,_)=9.,5.,9.,1.;

    //qDebug() << a;

    dtkDenseMatrix<double>::IndexVariable i, j;

    //a(i,j) *= b(i,j);

    a(_,1)(i) = b(_,1)(i);
    //qDebug() << a ;

}

void dtkMatrixOperationsTestCase::cleanupTestCase(void)
{

}

void dtkMatrixOperationsTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkMatrixOperationsTest, dtkMatrixOperationsTestCase)

//
// dtkMatrixTest.cpp ends here
