// Version: $Id$
//
//

// Commentary: FAKE CLASS FOR DOCUMENTATION ONLY
//
//

// Change Log:
//
//

// Code:

template <typename T> class dtkDenseVector
{
public:
    // typedef A                                   Engine;
    // typedef typename Engine::ElementType        ElementType;
    typedef T         ElementType;
    typedef qlonglong IndexType;
    // typedef typename Engine::IndexType          IndexType;

    // // view types from Engine
    // typedef typename Engine::ConstView          EngineConstView;
    // typedef typename Engine::View               EngineView;
    // typedef typename Engine::NoView             EngineNoView;

    // view types
    typedef flens::DenseVector<EngineConstView> ConstView;
    typedef flens::DenseVector<EngineView>      View;
    typedef flens::DenseVector<EngineNoView>    NoView;

private:
    typedef dtkDenseVector                          DV;

public:
    typedef flens::IndexVariable<IndexType>      IndexVariable;
    //typedef densevector::ConstElementClosure<DV> ConstElementClosure;
    //typedef densevector::ElementClosure<DV>      ElementClosure;
    typedef densevector::Initializer<DV>         Initializer;

public:
             dtkDenseVector(void);
    explicit dtkDenseVector(qlonglong length);
             // dtkDenseVector(qlonglong length, qlonglong firstIndex);
             // dtkDenseVector(const Range<qlonglong>& range);
             // dtkDenseVector(const Engine& engine, bool reverse=false);
             dtkDenseVector(const dtkDenseVector& rhs);

    // -- operators --------------------------------------------------------

    Initializer operator = (const T& value);

    dtkDenseVector& operator  = (const dtkDenseVector& rhs);
    dtkDenseVector& operator += (const dtkDenseVector& rhs);
    dtkDenseVector& operator -= (const dtkDenseVector& rhs);

    template <typename U> dtkDenseVector& operator += (const U& rhs);
    template <typename U> dtkDenseVector& operator -= (const U& rhs);
    template <typename U> dtkDenseVector& operator *= (const U& alpha);
    template <typename U> dtkDenseVector& operator /= (const U& alpha);

    const T& operator () (qlonglong index) const;
          T& operator () (qlonglong index);

    //template <typename S> const densevector::ConstElementClosure<dtkDenseVector, typename Scalar<S>::Impl> operator () (const Scalar<S>& index) const;

    const ConstElementClosure operator () (const dtkDenseVector::IndexVariable& i) const;
               ElementClosure operator () (      dtkDenseVector::IndexVariable& i);

    //-- views -------------------------------------------------------------

    const ConstView operator () (const dtkRange& range) const;
               View operator () (const dtkRange& range);

    // const ConstView operator () (const Range<qlonglong> &range, qlonglong firstViewIndex) const;
    //            View operator () (const Range<qlonglong> &range, qlonglong firstViewIndex);

    //const ConstView operator () (const Underscore<qlonglong> &all, qlonglong firstViewIndex) const;
    //           View operator () (const Underscore<qlonglong> &all, qlonglong firstViewIndex);

    const ConstView reverse(void) const;
               View reverse(void);

    // -- methods ----------------------------------------------------------

    //Range<qlonglong> range(void) const;

    qlonglong firstIndex(void) const;
    qlonglong  lastIndex(void) const;

    qlonglong endIndex(void) const;

    qlonglong length(void) const;

    const T *data(void) const;
          T *data(void);

    qlonglong stride(void) const;

    // qlonglong indexBase(void) const;

    //template <typename RHS> bool resize(const dtkDenseVector<RHS> &rhs, const ElementType &value = ElementType());

    //bool resize(qlonglong length, qlonglong firstIndex = Engine::defaultIndexBase, const ElementType &value = ElementType());
    bool resize(qlonglong length);

    //bool fill(const T& value = T(0));
    void fill(const T& value = T(0));

    //void changeIndexBase(qlonglong firstIndex);

    // -- implementation ---------------------------------------------------

    // const A& engine(void) const;
    //       A& engine(void);

    // bool reversed(void) const;
};

//
// dtkDenseVector.h ends here
