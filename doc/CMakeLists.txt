## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

string(SUBSTRING ${Qt5Core_VERSION_STRING} 0 3 Qt5_VERSION)

set(QT5_DOCUMENTATION_D "${QT5_BINARY_DIR}/../../../Docs/Qt-${Qt5_VERSION}")
set(QT5_QDOC_EXECUTABLE "${QT5_BINARY_DIR}/qdoc")
set(QT5_QHGN_EXECUTABLE "${QT5_BINARY_DIR}/qhelpgenerator")
set(DTK_DOCUMENTATION_D "${dtk_DIR}/doc")
set(DTK_QDOC_EXECUTABLE "${QT5_BINARY_DIR}/qdoc")

message(STATUS "Using Qt5 binary dir: ${QT5_BINARY_DIR}")
message(STATUS "Using dtk binary doc: ${DTK_QDOC_EXECUTABLE}")
message(STATUS "Using Qt5 binary qhg: ${QT5_QHGN_EXECUTABLE}")

## ###################################################################
## dtkLinearAlgebraDense
## ###################################################################

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/dtklinearalgebradense.qdocconf.in ${CMAKE_CURRENT_BINARY_DIR}/dtklinearalgebradensedoc.qdocconf IMMEDIATE @ONLY)

add_custom_target(doc
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMAND ${DTK_QDOC_EXECUTABLE} -no-link-errors dtklinearalgebradensedoc.qdocconf
  COMMAND ${QT5_QHGN_EXECUTABLE} ${dtk_INSTALL_DOCS}/dtklinearalgebradense/dtklinearalgebradense.qhp -o ${dtk_INSTALL_DOCS}/dtklinearalgebradense.qch
  COMMENT "-- Generating dtkLinearAlgebraDense documentation")

######################################################################
### CMakeLists.txt ends here
