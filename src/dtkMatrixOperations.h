// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkDenseVector.h"
#include "dtkDenseMatrix.h"

namespace dtk { namespace lapack {

    template < typename T > void pinv(dtkDenseMatrix<T>& a, dtkDenseMatrix<T>& res, double eps = 1e-20);

    template < typename T > double norm(const dtkDenseMatrix<T>& a);

    template < typename T > dtkDenseMatrix<T> solve(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& b); //return X such as AX=B
    template < typename T > dtkDenseVector<T> solve(const dtkDenseMatrix<T>& a, const dtkDenseVector<T>& b); //return x such as Ax=b

    template < typename T > void svd(dtkDenseMatrix<T>& a, dtkDenseVector<T>& s, dtkDenseMatrix<T>& u,  dtkDenseMatrix<T>& v);
    template < typename T > double cond(dtkDenseMatrix<T>& a);

    template < typename T > void lu(const dtkDenseMatrix<T>& a, dtkDenseMatrix<T>& l, dtkDenseMatrix<T>& u);

    template < typename T > dtkDenseMatrix<T>  lyap(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& q);  //return x such as AX+XA'+Q=0
    template < typename T > dtkDenseMatrix<T> dlyap(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& q); //return x such as AXA'-X+Q=0

    }

    template < typename T > dtkDenseMatrix<T> fromDiag(const dtkDenseVector<T>& v);
}

//
// dtkMatrixOperations.h ends here
