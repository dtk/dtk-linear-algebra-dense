// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkStaticVectorEngine.h"
#include "dtkStaticVectorEngineView.h"
#include "dtkStaticVectorEngineConstView.h"

#include <flens/flens.h>

// ///////////////////////////////////////////////////////////////////
// dtkFlensTinyVector alias
// ///////////////////////////////////////////////////////////////////

template < typename Eng > using dtkFlensTinyVector = flens::TinyVector<Eng>;

// ///////////////////////////////////////////////////////////////////
// dtkStaticVectorBase definition
// ///////////////////////////////////////////////////////////////////

template < typename Eng > class dtkStaticVectorBase : public dtkFlensTinyVector<Eng>
{
public:
    typedef Eng                          Engine;
    typedef typename Engine::ElementType ElementType;
    typedef typename Engine::IndexType   IndexType;

    typedef ElementType       value_type;
    typedef value_type*       pointer;
    typedef const value_type* const_pointer;
    typedef IndexType         size_type;

public:
    dtkStaticVectorBase(void);
    dtkStaticVectorBase(const Engine& engine);
    dtkStaticVectorBase(const dtkStaticVectorBase& rhs);
    dtkStaticVectorBase(std::initializer_list<value_type> list);

public:
    template < typename RHS > dtkStaticVectorBase(const dtkFlensTinyVector<RHS>& rhs);
    template < typename RHS > dtkStaticVectorBase(const flens::Vector<RHS>& rhs);

public:
    template < typename RHS, typename = typename std::enable_if<!std::is_same<Eng, RHS>::value>::type> dtkStaticVectorBase(dtkFlensTinyVector<RHS>&& rhs);

public:
    dtkStaticVectorBase& operator = (const dtkStaticVectorBase& rhs);

public:
    template < typename RHS > dtkStaticVectorBase& operator  = (const flens::Vector<RHS>& rhs);
    template < typename RHS > dtkStaticVectorBase& operator += (const flens::Vector<RHS>& rhs);
    template < typename RHS > dtkStaticVectorBase& operator -= (const flens::Vector<RHS>& rhs);

public:
    dtkStaticVectorBase& operator  = (const value_type& value);
    dtkStaticVectorBase& operator += (const value_type& value);
    dtkStaticVectorBase& operator -= (const value_type& value);
    dtkStaticVectorBase& operator *= (const value_type& value);
    dtkStaticVectorBase& operator /= (const value_type& value);

public:
    constexpr size_type   size(void) const;
    constexpr size_type  count(void) const;
    constexpr size_type length(void) const;

public:
    constexpr const_pointer data(void) const;
                    pointer data(void);

    constexpr const_pointer constData(void) const;
};

// ///////////////////////////////////////////////////////////////////
// dtkStaticVector alias
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size > using dtkStaticVector = dtkStaticVectorBase< dtkStaticVectorEngine<T, Size> >;
template < typename T, std::size_t Size > using dtkStaticVectorView = dtkStaticVectorBase< dtkStaticVectorEngineView<T, Size, 1UL> >;
template < typename T, std::size_t Size > using dtkStaticVectorConstView = dtkStaticVectorBase< dtkStaticVectorEngineConstView<T, Size, 1UL> >;

// ///////////////////////////////////////////////////////////////////
// dtkVector3D alias
// ///////////////////////////////////////////////////////////////////

template < typename T = double > using dtkVector3D = dtkStaticVector<T, 3LL>;
template < typename T = double > using dtkVector3DView = dtkStaticVectorView<T, 3LL>;
template < typename T = double > using dtkVector3DConstView = dtkStaticVectorConstView<T, 3LL>;

// ///////////////////////////////////////////////////////////////////
// Helpers
// ///////////////////////////////////////////////////////////////////

#include <QDebug>
#include <QDataStream>

template < typename Eng > QDebug& operator << (QDebug debug, const dtkStaticVectorBase<Eng>& vec);

template < typename T, std::size_t Size > QDataStream& operator << (QDataStream& s, const dtkStaticVector<T, Size>& vec);
template < typename T, std::size_t Size > QDataStream& operator >> (QDataStream& s,       dtkStaticVector<T, Size>& vec);

//
// dtkStaticVector.h ends here
