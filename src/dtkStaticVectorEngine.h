// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstddef>
#include <dtkCore/dtkStaticArray.h>

// ///////////////////////////////////////////////////////////////////
// dtkStaticVectorEngine storage class
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size, std::size_t BaseIndex = 1UL> class dtkStaticVectorEngine
{
public:
    typedef dtkStaticArray<T, Size> Data;
    Data d;
    T *ptr;

public:
    typedef T            ElementType;
    typedef std::size_t  IndexType;

    static const std::size_t length = Size;
    static const std::size_t stride = 1UL;
    static const std::size_t firstIndex = BaseIndex;
    static const std::size_t lastIndex = firstIndex + length - 1UL;

public:
     dtkStaticVectorEngine(void) : ptr(d.data() - firstIndex) {}
     dtkStaticVectorEngine(const dtkStaticVectorEngine& rhs) : d(rhs.d), ptr(d.data() - firstIndex) {}
    ~dtkStaticVectorEngine(void) {}

public:
    void fill(const T& val = T(0)) { d.fill(val); }

public:
    constexpr const T& operator () (std::size_t index) const { return *(ptr + index); }
                    T& operator () (std::size_t index)       { return *(ptr + index); }

public:
    constexpr const T *data(void) const { return d.data(); }
                    T *data(void)       { return d.data(); }

    constexpr const T *constData(void) const { return d.constData(); }
};

//
// dtkStaticVectorEngine.h ends here
