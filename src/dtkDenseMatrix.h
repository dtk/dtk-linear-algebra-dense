// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <flens/flens.h>
#include <dtkDenseVector.h>

// ///////////////////////////////////////////////////////////////////
// Alias dtkDenseMatrix
// ///////////////////////////////////////////////////////////////////

template < typename T > using dtkDenseMatrix = flens::GeMatrix< flens::FullStorage<T> >;

typedef dtkDenseMatrix<double>    dtkDenseMatrixReal;
typedef dtkDenseMatrix<long long> dtkDenseMatrixInt;

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template < typename Eng > QDebug& operator << (QDebug debug, const flens::GeMatrix<Eng>& mat);

template < typename T > QDataStream& operator << (QDataStream& s, const dtkDenseMatrix<T>& mat);
template < typename T > QDataStream& operator >> (QDataStream& s,       dtkDenseMatrix<T>& mat);

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

namespace dtk { namespace lapack{
        template < typename T > void    svd(dtkDenseMatrix<T>& a, dtkDenseVector<T>& s, dtkDenseMatrix<T>& u,  dtkDenseMatrix<T>& v);
        template < typename T > double cond(const dtkDenseMatrix<T>& a);
    };
};

//
// dtkDenseMatrix.h ends here
