// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkDenseMatrix.h"

#include <flens/flens.cxx>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template < typename Eng > inline QDebug& operator << (QDebug debug, const flens::GeMatrix<Eng>& mat)
{
    const bool oldSetting = debug.autoInsertSpaces();
    debug.nospace() << "DenseMatrix of size: " << mat.numRows() << " x " << mat.numCols() << '\n';
    debug.nospace() << "[ ";
    if (mat.numRows() > 0) {
        if (mat.numCols() > 0) {
            debug << mat(1, 1);
        }
        for (qlonglong j = 2; j <= mat.numCols(); ++j) {
            debug.nospace() << ", " << mat(1, j);
        }
    }

    for (qlonglong i = 2; i <= mat.numRows(); ++i) {
        debug.nospace() << '\n';
        if (mat.numCols() > 0) {
            debug << "  " << mat(i, 1);
        }
        for (qlonglong j = 2; j <= mat.numCols(); ++j) {
            debug.nospace() << ", " << mat(i, j);
        }
    }
    debug << " ]";
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();
}

template < typename T > inline QDataStream& operator << (QDataStream& s, const dtkDenseMatrix<T>& mat)
{
    s << quint64(mat.numRows()) << quint64(mat.numCols());
    const T *it = mat.data();
    qlonglong size = mat.numRows() * mat.numCols();
    const T *end = it + size;

    for (; it != end; ++it) {
        s << *it;
    }
    return s;
}

template < typename T > inline QDataStream& operator >> (QDataStream& s, dtkDenseMatrix<T>& mat)
{
    quint64 row_count; s >> row_count;
    quint64 col_count; s >> col_count;

    mat.resize(row_count, col_count);
    T *it = mat.data();
    T *end = it + row_count * col_count;

    for (; it != end; ++it) {
        s >> *it;
    }
    return s;
}
