// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstddef>

// ///////////////////////////////////////////////////////////////////
// dtkStaticVectorEngineConstView storage class
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size, std::size_t Inc, std::size_t BaseIndex = 1UL > class dtkStaticVectorEngineConstView
{
public:
    const T *d;

public:
    typedef T            ElementType;
    typedef std::size_t  IndexType;

    static const std::size_t length = Size;
    static const std::size_t stride = Inc;
    static const std::size_t firstIndex = BaseIndex;
    static const std::size_t lastIndex = firstIndex + length - 1LL;

public:
     dtkStaticVectorEngineConstView(const T *data) : d(data) {}
    ~dtkStaticVectorEngineConstView(void) {}

public:
    constexpr const T& operator () (std::size_t index) const { return d[index - firstIndex]; }

public:
    constexpr const T *data(void) const { return d; }

    constexpr const T *constData(void) const { return d; }
};

//
// dtkStaticVectorEngineConstView.h ends here
