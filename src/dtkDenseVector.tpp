// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkDenseVector.h"

#include <flens/flens.cxx>

// ///////////////////////////////////////////////////////////////////
// Helper functions implementations
// ///////////////////////////////////////////////////////////////////

template < typename T > inline const typename dtkDenseVector<T>::ConstView dtkDenseVectorFromRawData(qlonglong length, const T* raw_data)
{
    typedef typename dtkDenseVector<T>::EngineConstView ECV;
    typedef typename dtkDenseVector<T>::ConstView        CV;

    return CV(ECV(length, raw_data));
}

template < typename T > typename dtkDenseVector<T>::View dtkDenseVectorFromWritableRawData(qlonglong length, T* raw_data)
{
    typedef typename dtkDenseVector<T>::EngineView EV;
    typedef typename dtkDenseVector<T>::View        V;

    return V(EV(length, raw_data));
}

template < typename Eng > inline QDebug& operator << (QDebug debug, const flens::DenseVector<Eng>& vec)
{
    const bool oldSetting = debug.autoInsertSpaces();
    debug.nospace() << "DenseVector of size " << vec.length() << ":" << '\n';
    debug.nospace() << "[ ";
    if (vec.length() > 0)
        debug << vec(1);
    for (qlonglong i = 2; i <= vec.length(); ++i) {
        debug << ", ";
        debug << vec(i);
    }
    debug << " ]";
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();
}

template < typename T > inline QDataStream& operator << (QDataStream& s, const dtkDenseVector<T>& vec)
{
    s << quint64(vec.length());
    for (qlonglong i = 1; i <= vec.length(); ++i) {
        s << vec(i);
    }
    return s;
}

template < typename T > inline QDataStream& operator >> (QDataStream& s, dtkDenseVector<T>& vec)
{
    quint64 length; s >> length;
    vec.resize(length);
    T tmp;
    for (qlonglong i = 1; i <= length; ++i) {
        s >> tmp;
        vec(i) = tmp;
    }
    return s;
}

//
// dtkDenseVector.tpp ends here
