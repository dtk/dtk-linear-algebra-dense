// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <flens/flens.h>

// ///////////////////////////////////////////////////////////////////
// typedef
// ///////////////////////////////////////////////////////////////////

typedef typename flens::IndexBaseOne<>::IndexType dtkFlensIndexType;

typedef flens::Range<dtkFlensIndexType> dtkRange;

// 
// dtkRange.h ends here
