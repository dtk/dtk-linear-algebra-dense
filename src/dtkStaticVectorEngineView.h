// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <cstddef>

// ///////////////////////////////////////////////////////////////////
// dtkStaticVectorEngineView storage class
// ///////////////////////////////////////////////////////////////////

template < typename T, std::size_t Size, std::size_t Inc, std::size_t BaseIndex = 1UL > class dtkStaticVectorEngineView
{
public:
    T *d;

public:
    typedef T            ElementType;
    typedef std::size_t  IndexType;

    static const std::size_t length = Size;
    static const std::size_t stride = Inc;
    static const std::size_t firstIndex = BaseIndex;
    static const std::size_t lastIndex = firstIndex + length - 1LL;

public:
     dtkStaticVectorEngineView(void) : d(0) {}
     dtkStaticVectorEngineView(T *data) : d(data) {}
     dtkStaticVectorEngineView(const dtkStaticVectorEngineView& rhs) : d(const_cast<dtkStaticVectorEngineView&>(rhs).d) {}
    ~dtkStaticVectorEngineView(void) {}

public:
    void fill(const T& val = T(0)) { std::fill_n(d, Size, val); }

public:
    constexpr const T& operator () (std::size_t index) const { return d[index - firstIndex]; }
                    T& operator () (std::size_t index)       { return d[index - firstIndex]; }

public:
    constexpr const T *data(void) const { return d; }
                    T *data(void)       { return d; }

    constexpr const T *constData(void) const { return d; }
};

//
// dtkStaticVectorEngineView.h ends here
