// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkStaticVector.h"

#include <flens/flens.cxx>

#include <QtGlobal>
#include <type_traits>

// ///////////////////////////////////////////////////////////////////
// dtkStaticVectorBase implementation
// ///////////////////////////////////////////////////////////////////

template < typename Eng > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(void) : dtkFlensTinyVector<Eng>()
{

}

template < typename Eng > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(const Engine& engine) : dtkFlensTinyVector<Eng>(engine)
{

}

template < typename Eng > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(const dtkStaticVectorBase& rhs) : dtkFlensTinyVector<Eng>(rhs)
{

}

template < typename Eng > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(std::initializer_list<dtkStaticVectorBase::value_type> list) : dtkFlensTinyVector<Eng>()
{
    Q_ASSERT_X((this->size() == list.size()), "dtkStaticVectorBase(std::initializer_list<T> list)", "Input initializer list has not the same size than the vector.");

    Eng& eng = this->engine();

    auto it = list.begin();
    for (qlonglong i = 0; i < this->size(); ++i, ++it) {
        eng.data()[i] = *it;
    }
}

template < typename Eng > template < typename RHS > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(const dtkFlensTinyVector<RHS>& rhs) : dtkFlensTinyVector<Eng>()
{
    const int rhs_size = RHS::length;
    const int this_size = Engine::length;
    static_assert(this_size == rhs_size, "Input rhs vector has not the right size.");

    Eng& eng = this->engine();
    for (qlonglong i = 0; i < this->size(); ++i) {
        eng.data()[i] = rhs(i + 1);
    }

}

template < typename Eng > template < typename RHS > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(const flens::Vector<RHS>& rhs) : dtkFlensTinyVector<Eng>(rhs)
{

}

template < typename Eng > template < typename RHS, typename > inline dtkStaticVectorBase<Eng>::dtkStaticVectorBase(dtkFlensTinyVector<RHS>&& rhs) : dtkFlensTinyVector<Eng>(rhs)
{

}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator = (const dtkStaticVectorBase& rhs)
{
    this->dtkFlensTinyVector<Eng>::operator = (rhs); return *this;
}

template < typename Eng > template < typename RHS > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator  = (const flens::Vector<RHS>& rhs)
{
    this->dtkFlensTinyVector<Eng>::operator = (rhs);  return *this;
}

template < typename Eng > template < typename RHS > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator += (const flens::Vector<RHS>& rhs)
{
    this->dtkFlensTinyVector<Eng>::operator += (rhs);  return *this;
}

template < typename Eng > template < typename RHS > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator -= (const flens::Vector<RHS>& rhs)
{
    this->dtkFlensTinyVector<Eng>::operator -= (rhs);  return *this;
}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator = (const value_type& value)
{
    this->engine().fill(value); return *this;
}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator += (const value_type& value)
{
    this->dtkFlensTinyVector<Eng>::operator += (value);  return *this;
}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator -= (const value_type& value)
{
    this->dtkFlensTinyVector<Eng>::operator -= (value);  return *this;
}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator *= (const value_type& value)
{
    this->dtkFlensTinyVector<Eng>::operator *= (value);  return *this;
}

template < typename Eng > inline dtkStaticVectorBase<Eng>& dtkStaticVectorBase<Eng>::operator /= (const value_type& value)
{
    this->dtkFlensTinyVector<Eng>::operator /= (value);  return *this;
}

template < typename Eng > inline constexpr typename dtkStaticVectorBase<Eng>::size_type dtkStaticVectorBase<Eng>::size(void) const
{
    return this->dtkFlensTinyVector<Eng>::length();
}

template < typename Eng > inline constexpr typename dtkStaticVectorBase<Eng>::size_type dtkStaticVectorBase<Eng>::count(void) const
{
    return this->dtkFlensTinyVector<Eng>::length();
}

template < typename Eng > inline constexpr typename dtkStaticVectorBase<Eng>::size_type dtkStaticVectorBase<Eng>::length(void) const
{
    return this->dtkFlensTinyVector<Eng>::length();
}

template < typename Eng > inline constexpr typename dtkStaticVectorBase<Eng>::const_pointer dtkStaticVectorBase<Eng>::data(void) const
{
    return this->engine().data();
}

template < typename Eng > inline typename dtkStaticVectorBase<Eng>::pointer dtkStaticVectorBase<Eng>::data(void)
{
    return this->engine().data();
}

template < typename Eng > inline constexpr typename dtkStaticVectorBase<Eng>::const_pointer dtkStaticVectorBase<Eng>::constData(void) const
{
    return this->engine().data();
}

// ///////////////////////////////////////////////////////////////////
// Helpers implementation
// ///////////////////////////////////////////////////////////////////

template < typename Eng > inline QDebug& operator << (QDebug debug, const dtkStaticVectorBase<Eng>& vec)
{
    typedef typename dtkStaticVectorBase<Eng>::value_type T;

    const bool oldSetting = debug.autoInsertSpaces();
    debug.nospace() << QString("dtkStaticVector<%1,%2> : ").arg(QMetaType::typeName(qMetaTypeId<T>())).arg(vec.size());
    debug.nospace() << "[ ";
    if (vec.size() > 0)
        debug << vec(1);
    for (qlonglong i = 2; i <= vec.size(); ++i) {
            debug << ", ";
        debug << vec(i);
    }
    debug << " ]";
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();

}

template < typename T, qlonglong Size > inline QDataStream& operator << (QDataStream& s, const dtkStaticVector<T, Size>& vec)
{
    for (qlonglong i = 1; i <= vec.size(); ++i) {
        s << vec(i);
    }
    return s;
}

template < typename T, qlonglong Size > inline QDataStream& operator >> (QDataStream& s, dtkStaticVector<T, Size>& vec)
{
    vec;
    T tmp;
    for (qlonglong i = 1; i <= vec.size(); ++i) {
        s >> tmp;
        vec(i) = tmp;
    }
    return s;
}

//
// dtkStaticVectorBase.tpp ends here
