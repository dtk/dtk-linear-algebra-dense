// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#ifndef USE_CXXLAPACK
#define USE_CXXLAPACK
#endif

#include "dtkMatrixOperations.h"

#include "dtkDenseVector.tpp"
#include "dtkDenseMatrix.tpp"

#include <flens/flens.cxx>

using namespace flens;
using namespace std;

namespace dtk { namespace lapack {

    template < typename T > inline void pinv(dtkDenseMatrix<T>& a, dtkDenseMatrix<T>& res, double eps)
    {
        qlonglong mainDim = qMax(a.numRows(), a.numCols());
        qlonglong secDim  = qMin(a.numRows(), a.numCols());
        dtkDenseMatrix<T> u(a.numRows(), a.numRows()), v(a.numCols(), a.numCols());
        dtkDenseVector<T> s(secDim);

        dtk::lapack::svd(a, s, u, v);

        double epsilon = norm(a) * eps;

        dtkDenseMatrix<T> sMatrix(a.numCols(), a.numRows());
        for(int i=1; i < s.length() + 1; ++i) {
            if(s(i) > epsilon)
                sMatrix(i,i) = 1 / s(i);
        }

        dtkDenseMatrix<T> subres(a.numCols(), a.numRows());
        subres = transpose(v) * sMatrix;
        res = subres * transpose(u);
    }

    template < typename T > inline double norm(const dtkDenseMatrix<T>& a)
    {
        qlonglong n = a.numCols();
        DenseVector< Array<T> > res(n), resi(n);

        dtkDenseMatrix<T> ata(a.numCols(), a.numCols());
        ata = transpose(a) * a;

        flens::lapack::ev(ata.impl(), res, resi);

        int min = 1, max = 1;

        for(int i = 1; i <= n; ++i) {
            if(res(i) > res(max))
                max = i;
        }
        return sqrt(res(max));
    }

    template < typename T > inline dtkDenseMatrix<T> solve(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& b)
    { //return X such as AX=B

    }

    template < typename T > inline dtkDenseVector<T> solve(const dtkDenseMatrix<T>& a, const dtkDenseVector<T>& b) //return x such as Ax=b
    {
        if (a.numRows() == a.numCols()) {
            dtkDenseVector<double> cloneB(b);
            dtkDenseMatrix<double> cloneA(a);
            DenseVector<Array<qlonglong> > piv(b.length());

            flens::lapack::sv(cloneA.impl(), piv, cloneB.impl());
            return cloneB;
        }

        dtkDenseVector<double> cloneB(b);
        dtkDenseMatrix<double> cloneA(a);

        flens::lapack::ls(NoTrans, cloneA.impl(), cloneB.impl());
        return cloneB;
    }

    template < typename T > inline void svd(dtkDenseMatrix<T>& a, dtkDenseVector<T>& s, dtkDenseMatrix<T>& u,  dtkDenseMatrix<T>& v)
    {
        flens::lapack::svd(flens::lapack::SVD::Job::All,flens::lapack::SVD::Job::All, a.impl(), s.impl(), u.impl(), v.impl());
    }

    template < typename T > inline double cond(dtkDenseMatrix<T>& a)
    {
        qlonglong n = a.numCols();
        DenseVector< Array<T> > res(n), resi(n);

        flens::lapack::ev(a.impl(), res, resi);

        int min = 1, max = 1;

        for(int i = 1; i <= n; ++i) {
            if(res(i) < res(min))
                min = i;
            if(res(i) > res(max))
                max = i;
        }
        return (res(max) / res(min));
    }

    template < typename T > inline void lu(const dtkDenseMatrix<T>& a, dtkDenseMatrix<T>& l, dtkDenseMatrix<T>& u)
    {
        const dtkUnderscore _;
        dtkDenseMatrix<double> ab(a);
        dtkDenseVector<qlonglong>  piv(a.numRows());

        l.fill(0);
        u.fill(0);

        flens::lapack::trf(ab.impl(), piv.impl());

        for(int j = 1; j < ab.numCols() + 1; ++j) {
            for(int i = 1 ; i < ab.numRows() + 1; ++i) {
                if(i > j)
                    l(i,j) = ab(i,j);
                else
                    u(i,j) = ab(i,j);
                if(i == j)
                    l(i,j) = 1;
            }
        }
    }

    template < typename T > inline dtkDenseMatrix<T> lyap(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& q)  //return x such as AX+XA'+Q=0
    {

    }

    template < typename T > inline dtkDenseMatrix<T> dlyap(const dtkDenseMatrix<T>& a, const dtkDenseMatrix<T>& q) //return x such as AXA'-X+Q=0
    {

    }

} // end lapack

    template < typename T > inline dtkDenseMatrix<T> fromDiag(const dtkDenseVector<T>& v)
    {
        dtkDenseMatrix<T> mat(v.length(), v.length());
        mat.diag(0) = v;
        return mat;
    }

} // end dtk

//
// dtkMatrixOperations.tpp ends here
