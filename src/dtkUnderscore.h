// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:


#pragma once

#include <flens/flens.h>

// ///////////////////////////////////////////////////////////////////
// typedef
// ///////////////////////////////////////////////////////////////////

typedef typename flens::IndexBaseOne<>::IndexType dtkFlensIndexType;

typedef flens::Underscore<dtkFlensIndexType> dtkUnderscore;

// 
// dtkUnderscore.h ends here
