// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <flens/flens.h>

// ///////////////////////////////////////////////////////////////////
// Alias dtkDenseVector
// ///////////////////////////////////////////////////////////////////

template < typename T > using dtkDenseVector = flens::DenseVector< flens::Array<T> >;

typedef dtkDenseVector<double>    dtkDenseVectorReal;
typedef dtkDenseVector<long long> dtkDenseVectorInt;

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template < typename T > const typename dtkDenseVector<T>::ConstView dtkDenseVectorFromRawData(qlonglong length, const T* raw_data);
template < typename T >       typename dtkDenseVector<T>::View      dtkDenseVectorFromWritableRawData(qlonglong length, T* raw_data);

template < typename Eng > QDebug& operator << (QDebug debug, const flens::DenseVector<Eng>& vec);

template < typename T > QDataStream& operator << (QDataStream& s, const dtkDenseVector<T>& vec);
template < typename T > QDataStream& operator >> (QDataStream& s,       dtkDenseVector<T>& vec);

//
// dtkDenseVector.h ends here
