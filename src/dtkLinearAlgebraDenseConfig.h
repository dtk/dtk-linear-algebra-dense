// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkConfig>

// ///////////////////////////////////////////////////////////////////
// Configure size of integer for FLENS
// ///////////////////////////////////////////////////////////////////

#ifdef FLENS_DEFAULT_INDEXTYPE
#undef FLENS_DEFAULT_INDEXTYPE
#endif

#ifdef INTEGER
#undef INTEGER
#endif

#ifdef DTK_BUILD_64
#      define FLENS_DEFAULT_INDEXTYPE long long
#      define INTEGER long long
#else
#      define FLENS_DEFAULT_INDEXTYPE int
#      define INTEGER int
#endif

// 
// dtkLinearAlgebraDenseConfig.h ends here
