## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

set(dtk_BLAS_LAPACK_LIBS "")

if(NOT APPLE)

  set(BLA_VENDOR "OpenBLAS" CACHE STRING "Selected BLAS library (e.g. OpenBLAS, MKL, Atlas)")
  set_property(CACHE BLA_VENDOR PROPERTY STRINGS "Atlas;Open;MKL")

  if(BLA_VENDOR STREQUAL "Open" OR BLA_VENDOR STREQUAL "open" OR BLA_VENDOR STREQUAL "openblas" OR BLA_VENDOR STREQUAL "OpenBLAS")
    find_package(OpenBLAS REQUIRED)
    include_directories(SYSTEM ${OpenBLAS_INCLUDE_DIR})
    list(APPEND dtk_BLAS_LAPACK_LIBS ${OpenBLAS_LIB})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DWITH_OPENBLAS")
    if(OpenBLAS_FOUND)
      set(BLAS_FOUND ON)
    endif()

  elseif(BLA_VENDOR STREQUAL "Atlas" OR BLA_VENDOR STREQUAL "ATLAS" OR BLA_VENDOR STREQUAL "atlas")
    find_package(Atlas REQUIRED)
    include_directories(SYSTEM ${Atlas_INCLUDE_DIR})
    list(APPEND dtk_BLAS_LAPACK_LIBS ${Atlas_LIBRARIES})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DWITH_ATLAS")

  elseif(BLA_VENDOR STREQUAL "MKL" OR BLA_VENDOR STREQUAL "mkl")
    find_package(BLAS REQUIRED)
    list(APPEND dtk_BLAS_LAPACK_LIBS ${BLAS_LIBRARIES})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DWITH_MKLBLAS")
    find_package(LAPACK REQUIRED)
    list(APPEND dtk_BLAS_LAPACK_LIBS ${LAPACK_LIBRARIES})

  else()
    unset(BLA_VENDOR)
    find_package(BLAS REQUIRED)
    list(APPEND dtk_BLAS_LAPACK_LIBS ${BLAS_LIBRARIES})
    find_package(LAPACK REQUIRED)
    list(APPEND dtk_BLAS_LAPACK_LIBS ${LAPACK_LIBRARIES})

  endif()

elseif(APPLE)

  find_package(vecLib REQUIRED)
  include_directories(SYSTEM ${vecLib_INCLUDE_DIR})
  list(APPEND dtk_BLAS_LAPACK_LIBS ${vecLib_LINKER_LIBS})
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DWITH_VECLIB")

endif()

######################################################################
### Dependencies.cmake ends here
